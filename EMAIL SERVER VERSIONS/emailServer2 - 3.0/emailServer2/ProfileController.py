#!/usr/bin/env python
import Cookie
import cgi
import os
from profileview import ProfileView

#logger
import logging
FORMAT = "[%(filename)s:%(lineno)s - %(funcName)10s() ] %(message)s"
logging.basicConfig(filename='TEST.log',format=FORMAT)
logger = logging.getLogger('root')
logger.setLevel(logging.DEBUG)


class ProfileController:

    def __init__(self):
        cookie = Cookie.SimpleCookie()
        cookie_string = os.environ.get('HTTP_COOKIE')
        cookie.load(cookie_string)
        userid = cookie['userid'].value
        password = cookie['password'].value
                                                        

        form = cgi.FieldStorage()
        logout = form.getvalue('logout')

        viewProfile = ProfileView()
        
        if logout in form:
            #expire cookies, then logout
            cookie['userid']['expires']="Thu, 01 Jan 1970 00:00:00 GMT"
            cookie['password']['expires']="Thu, 01 Jan 1970 00:00:00 GMT"

            viewProfile.redir()
                                         
        else:
            viewProfile.view(userid)
            
controller = ProfileController()
