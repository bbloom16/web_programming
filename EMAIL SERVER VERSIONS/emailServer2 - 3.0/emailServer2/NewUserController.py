#!/usr/bin/env python
import Cookie
import cgi
import os
from userdao import UserDao
from newuserview import NewUserView

#logger
import logging
FORMAT = "[%(filename)s:%(lineno)s - %(funcName)10s() ] %(message)s"
logging.basicConfig(filename='newUserController.log',format=FORMAT)
logger = logging.getLogger('root')
logger.setLevel(logging.DEBUG)

class NewUserController:                        
    
    def __init__(self):
        # get userid/password if this is a form submit
        form = cgi.FieldStorage()
        userid   = form.getvalue('userid')
        password = form.getvalue('password')
        submit = form.getvalue('newuser')
        
        userDAO = UserDao()
        newUserV = NewUserView()

        if (submit in form) and (userDAO.selectByUserId(userid) is None):
            userDAO.makeUser(userid, password)
            newUserV.redir()
        else:
            newUserV.view()
                                                
controller = NewUserController()
