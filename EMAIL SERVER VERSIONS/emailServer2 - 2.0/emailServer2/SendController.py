#!/usr/bin/env python

import cgi
import cgitb
import sys
import Cookie
import os
import base64
from sendview import SendView
from maildao import MailDao
from userdao import UserDao
#logger
import logging
FORMAT = "[%(filename)s:%(lineno)s - %(funcName)10s() ] %(message)s"
logging.basicConfig(filename='sendController.log',format=FORMAT)
logger = logging.getLogger('root')
logger.setLevel(logging.DEBUG)

class SendController:
    def __init__(self):
        cookie = Cookie.SimpleCookie()
        cookie_string = os.environ.get('HTTP_COOKIE')
        cookie.load(cookie_string)
        userid = cookie['userid'].value
        password = cookie['password'].value

        sendV = SendView()
        mailDao = MailDao(userid)
        userDao = UserDao()

        form = cgi.FieldStorage()
        send = form.getvalue('send')

        if "send" in form:
            recip = form.getvalue('userlist')
            body = form.getvalue('body')
            
            mailDaoRecip = MailDao(recip)

            #add to sender's outbox
            mailDao.addOutbox(body)
            #add to recip's inbox
            mailDaoRecip.addInbox(body)
            
            sendV.refresh()
        else:
            users = userDao.readNoPass()
            recips = sendV.printUsers(users)                        
            sendV.view(userid, recips)

controller = SendController()
