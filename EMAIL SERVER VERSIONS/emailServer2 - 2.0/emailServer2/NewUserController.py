#!/usr/bin/env python
import Cookie
import cgi
import os
from userdao import UserDao
from newuserview import NewUserView

#logger
import logging
FORMAT = "[%(filename)s:%(lineno)s - %(funcName)10s() ] %(message)s"
logging.basicConfig(filename='newUserController.log',format=FORMAT)
logger = logging.getLogger('root')
logger.setLevel(logging.DEBUG)

class NewUserController:                        
    
    def __init__(self):
        # get userid/password if this is a form submit
        form = cgi.FieldStorage()
        userid   = form.getvalue('userid')
        password = form.getvalue('password')
        submit = form.getvalue('newuser')
        
        userDAO = UserDao()
        newUserV = NewUserView()

        if (submit in form) and (userDAO.selectByUserId(userid) is None):
            self.makeUser(userid, password)
            newUserV.redir()
        else:
            newUserV.view()

    #temporary workaround
    def makeUser(self, userid, password):
        f = open('users.txt','a')
        f.write(str(userid) + ' ' + str(password)+ '\n')
        f.close()
        #make inbox and outbox files for that user
        f1 = open(str(userid) + 'MailIn.txt','w+')
        f2 = open(str(userid) + 'MailOut.txt','w+')
                                                
controller = NewUserController()
