#!/usr/bin/env python
import Cookie
import base64

class LoginView:

#
# redirect()
#
    def redirect(self,cookie):
#        NOTE: Could not get straight redirects to work
#        print "Status: 201 (Created)"
#        print "Location: http://54.191.137.34:5000/survey.py"
#        print
        print "Content-type: text/html"
        print cookie.output()
        print 
        print "<!doctype html>"
        print "<html>"
        print "<head>"
        print "<meta http-equiv='Refresh' content='0; url=ProfileController.py' />"
        print "</head>"
        print "<body>"
        print "<p>Please follow <a href='ProfileController.py'>this link</a>.</p>"
        print "</body>"
        print "</html>"
        print
        print

#
# view()
#
    def view(self):
        jpgdata = base64.b64encode(open("billTed.jpg",'rb').read())

        print "Content-type: text/html"
        print
        print "<!doctype html>"
        print "<head>"
        print "<title>Brian's e-mail Server</title>"
        print "<link type='text/css' rel='stylesheet' href='./css/style.css'>"
        print "</head>"
        print "<body>"
        print "<h1>Welcome to Brian's e-mail server</h1>"
        print "<img src='data:image/jpg;base64,%s' />" % jpgdata
        print "<form id='form' method='post' >"
        print "   <p>User ID:   <input type='text' name='userid' /> </p>"
        print "   <p>Password: <input type='password' name='password' /> </p>"
        print "   <input type='submit' name='submit' value='submit'>"
        print "</form>"
        print "<h4><a href='NewUserController.py'>New User</a></h4>"
        print "</body>"
        print "</html>"
        
#login failed
    def loginFailed():
        #couldn't auth user
        print "Content-type: text/html"
        print
        print "<!doctype html>"
        print "<head>"
        print "<title>Error</title>"
        print "</head>"
        print "<body>"
        print "<h1>You have reached this page in error.  Click <a href='LoginController.py' action='clear.py'>here</a> to login again.</h1>"
        print "</body>"
        print "</html>"
        
