#!/usr/bin/env python
import Cookie
import cgi
from userdao import UserDao
from loginview import LoginView
class LoginController:
#
# Contstructor
#
    def __init__(self):
        # get userid/password if this is a form submit
        form = cgi.FieldStorage() 
        userid   = form.getvalue('userid')
        password = form.getvalue('password')

        # setup view
        view = LoginView()

        # form submit? redirect
        if self.isValid(userid,password):
            cookie = Cookie.SimpleCookie()
            cookie['userid']=userid
            cookie['password']=password
            
            view.redirect(cookie)
        # not form submit?  display form
        else:
            view.view()
#
# isValid()
#
    def isValid(self,userid, password):
        dao = UserDao()
        user = dao.selectByUserId(userid)
        return (user is not None) and (user.password == password)

#
# Main Program
#
controller = LoginController()
