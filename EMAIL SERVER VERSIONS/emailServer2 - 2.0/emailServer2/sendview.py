#!/usr/bin/env python
import Cookie
import base64
import os
import sys
import cgi
#logger
import logging
FORMAT = "[%(filename)s:%(lineno)s - %(funcName)10s() ] %(message\)s"
logging.basicConfig(filename='sendview.log',format=FORMAT)
logger = logging.getLogger('root')
logger.setLevel(logging.DEBUG)

class SendView:

    def view(self, userid, recips):
        print "Content-type: text/html"
        print
        print "<!doctype html>"
        print "<head>"
        print "<title>Send e-Mail</title>"
        print "<link type= 'text/css' rel='stylesheet' href= './css/style.css'>"
        print "</head>"
        print "<body>"
        #identify user by cookie
        print "<h1>Welcome " + str(userid) + "!</h1>"
        print "<h3>Send an email to a user in our system:</h3>"
        print "<ul>"
        print "   <li class='navlist'><a href='InboxController.py'>Inbox</a></li> &nbsp; | &nbsp; "
        print "   <li class='navlist'><a href='OutboxController.py'>Sent Mail</a></li>&nbsp; | &nbsp;  "
        print "   <li class='navlist'><a href='SendController.py'>Send Message</a></li> "
        print "</ul>"
        #print the users to select recipiant
        print "<form id='sendEmail' method='post'>"
        print "<p>Recipiant: " + str(recips) + "</p>"
        #text area body
        print "<br /><textarea name='body' rows='10' cols='50'>Enter text for email here...</textarea>"
        #send email button
        print "<br /><input type='submit' id='send' form='sendEmail' name='send' value='Send' />"
        print "</form>"
        print """                                                                                
        <br>                                                                                     
        <h4><a href='ProfileController.py'>My profile</a></h4>                                             
        """
        print "</body>"
        print "</html>"
        
        
    def refresh(self):
        print "Content-type: text/html"
        print
        print "<!doctype html>"
        print "<html>"
        print "<head>"
        print "<meta http-equiv='Refresh' content='0; url=SendController.py' />"
        print "</head>"
        print "</html>"
        print
        print

    def printUsers(self, users):
        usersPrint = '<select name="userlist">'
        for user in users:
            usersPrint += '<option value="'+ user+ '">' + user + '</option>'
        usersPrint += '</select>'
        return usersPrint
