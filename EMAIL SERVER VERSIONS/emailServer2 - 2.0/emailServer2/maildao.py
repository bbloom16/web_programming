#!/usr/bin/env python
import Cookie
import base64
import os
import sys
import cgi
from mail import Mail

#logger
import logging
FORMAT = "[%(filename)s:%(lineno)s - %(funcName)10s() ] %(message\
)s"
logging.basicConfig(filename='inboxview.log',format=FORMAT)
logger = logging.getLogger('root')
logger.setLevel(logging.DEBUG)


class MailDao:
    def __init__(self, userid):
        mail = Mail(userid)
        self.inbox = mail.inbox
        self.outbox = mail.outbox

    def readInbox(self):
        result = []
        with open(self.inbox, 'r') as file:
            # read the file into lines
            lines = file.readlines()
            
            # iterate through lines, splitting each line into strings
            for line in lines:
                #raw = line.split()
                result.append(line)
                
        return result
            
    
    def readOutbox(self):
        result = []
        with open(self.outbox, 'r') as file:
            # read the file into lines
            lines = file.readlines()
            
            # iterate through lines, splitting each line into strings
            for line in lines:
                #raw = line.split()
                result.append(line)
                
        return result
                                            
    def addInbox(self, email):
        f = open(self.inbox,'a')
        f.write(email + "\n")
        f.close()

    def addOutbox(self, email):
        f = open(self.outbox,'a')
        f.write(email + "\n")
        f.close()

    def deleteInbox(self, emailSelected):
        #logger.debug(emailSelected)
        emails = self.readInbox()
        with open(self.inbox, 'w') as file:
            count = 1
            for email in emails:
                if (count is not int(emailSelected)):
                    file.write(str(email))
                count = int(count + 1)        

    def deleteOutbox(self, emailSelect):
        #logger.debug(emailSelect)
        emails = self.readOutbox()
        with open(self.outbox, 'w') as file:
            count = 1
            for email in emails:
                if (count is not int(emailSelect)):
                    file.write(str(email))
                count = int(count + 1)
