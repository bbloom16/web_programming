#!/usr/bin/env python
 
import cgi
import cgitb
import sys
import Cookie


#
# isValid()
#

def isValid(userid, password):
    with open('users.txt', 'r') as file:
        # read the file into lines
        lines = file.readlines() 

        # iterate through lines, splitting each line into strings
        for line in lines:
            strings = iter(line.split())

            # process each string pair, return True if match
            # otherwise when end of file reached, drop to return False
            while True:
                try:
                    fileUserId = strings.next()
                    filePassword = strings.next()

                    # we have a match
                    if (userid == fileUserId):
                        return True

                except StopIteration:
                    break

    #user not in system, add them
    if (userid != None):
        f = open('users.txt','a')
        f.write(str(userid) + ' ' + str(password)+ '\n')
        f.close()
        #make inbox and outbox files for that user
        f1 = open(str(userid) + 'MailIn.txt','w+')
        f2 = open(str(userid) + 'MailOut.txt','w+')
        
        print "Content-type: text/html"
        print
        print "<!doctype html>"
        print "<html>"
        print "<head>"
        print "<meta http-equiv='Refresh' content='0; url=login.py' />"
        print "</head>"
        print "</html>"

        
    return False

#
# Main Program
#
def main():
    form1 = cgi.FieldStorage() 
    userid   = form1.getvalue('userid')
    password = form1.getvalue('password')

    #if user already exits in system, prompt for new username
    if isValid(userid, password):
        print "Content-type: text/html"
        print 
        print "<!doctype html>"
        print "<html>"
        print "<head>"
        print "<title>Error</title>"
        print "</head>"
        print "<body>"
        print "<p>Error, there is already a user in the system with that username, <a href='newUser.py'>try again</a>.</p>"
        print "</body>"
        print "</html>"

    else:
        print "Content-type: text/html"
        print
        print "<!doctype html>"
        print "<head>"
        print "<title>Brian's e-mail Server</title>"
	print "<link type='text/css' rel='stylesheet' href='./css/style.css'>"  
        print "</head>"
        print "<body>"
        print "<h3>Enter new user and password below: </h3>"
        print "<form id='form1' method='post' >"
        print "   <p>User ID:   <input type='text' name='userid' /> </p>"
        print "   <p>Password: <input type='password' name='password' /> </p>"
        print "   <input type='submit' name='submitNew' value='Create User'>"
        print "</form>"
        print "<h4><a href='login.py'>Login</a>"
        print "</body>"
        print "</html>"

main()
