#!/usr/bin/env python
import Cookie
import cgi
import os
from outboxview import OutboxView
from maildao import MailDao
#logger
import logging
FORMAT = "[%(filename)s:%(lineno)s - %(funcName)10s() ] %(message\
)s"
logging.basicConfig(filename='outboxController.log',format=FORMAT)
logger = logging.getLogger('root')
logger.setLevel(logging.DEBUG)


class OutboxController:
    def __init__(self):
        cookie = Cookie.SimpleCookie()
        cookie_string = os.environ.get('HTTP_COOKIE')
        cookie.load(cookie_string)
        userid = cookie['userid'].value
        password = cookie['password'].value

        outboxV = OutboxView()
        mailDao = MailDao(userid)
        
        form = cgi.FieldStorage()
        emailSelect = form.getvalue('emailSelect')
        view = form.getvalue('view')
        delete = form.getvalue('delete')

        if(emailSelect is not None):
            cookie['emailSelect'] = emailSelect

            if "view" in form:
                emails = mailDao.readOutbox()
                outboxV.viewEmail(emailSelect, emails)
            elif "delete" in form:
                mailDao.deleteOutbox(emailSelect)
                outboxV.refresh()
        else:
            emails = mailDao.readOutbox()
            result = outboxV.printEmails(emails)
            outboxV.view(userid, emails, result)        
        
        
controller = OutboxController()
