#!/usr/bin/env python
import Cookie
import base64
import os
import sys
import cgi
#logger
import logging
FORMAT = "[%(filename)s:%(lineno)s - %(funcName)10s() ] %(message)s"
logging.basicConfig(filename='profileview.log',format=FORMAT)
logger = logging.getLogger('root')
logger.setLevel(logging.DEBUG)

class ProfileView:

#view
    def view(self, userid):
                                
        pngdata = base64.b64encode(open("box.png",'rb').read())
            
        print "Content-type: text/html"
        print
        print "<!doctype html>"
        print "<head>"
        print "<title>Profile</title>"
        print "<link type= 'text/css' rel='stylesheet' href= './css/style.css'>"
        print "</head>"
        print "<body>"
        print "<h1>Welcome " + str(userid) + "!</h1>"
        print "<img src='data:image/png;base64,%s' />" % pngdata
        print "<h3>What's in the (in)box?</h3>"
        print "<ul>"
        print "   <li class='navlist'><a href='InboxController.py'>Inbox</a></li> &nbsp; | &nbsp; "
        print "   <li class='navlist'><a href='OutboxController.py'>Outbox</a></li>&nbsp; | &nbsp;  "
        print "   <li class='navlist'><a href='SendController.py'>Send Email</a></li> "
        print "</ul>"
        print "<form method='post'>"
        print "<br /><input type='submit' id='logout' name='logout' value='logout' />"
        print "</form>"
        print "</body>"
        print "</html>"
        
#redir to login method
    def redir(self):
        print "Content-type: text/html"
        print
        print "<!doctype html>"
        print "<html>"
        print "<head>"
        print "<meta http-equiv='Refresh' content='0; url=LoginController.py' />"
        print "</head>"
        print "<body>"
        print "<p>Please follow <a href='LoginController.py'>this link</a>.</p>"
        print "</body>"
        print "</html>"
        print
        print
        
