#!/usr/bin/env python

import cgi
import cgitb
import sys
import Cookie
import os
import base64

#auth user from cookies passed
def isValid(userid, password):
    with open('users.txt','r') as file:
        lines = file.readlines()

        for line in lines:
            strings = iter(line.split())
            while True:
                try:
                    fileUserId = strings.next()
                    filePass = strings.next()

                    if(userid == fileUserId) and (password == filePass):
                        return True
                except StopIteration:
                    break
    return False

def main():

    cookie = Cookie.SimpleCookie()
    cookie_string = os.environ.get('HTTP_COOKIE')
    cookie.load(cookie_string)
    userid = cookie['userid'].value
    password = cookie['password'].value


    if isValid(userid, password):
        #syntax for image
        pngdata = base64.b64encode(open("box.png",'rb').read())

        print "Content-type: text/html"
        print
        print "<!doctype html>"
        print "<head>"
        print "<title>Profile</title>"
        #ISSUE: Can't link css sheet
        print "<link type= 'text/css' rel='stylesheet' href= './css/style.css'>"    
        print "</head>"
        print "<body>"
        print "<h1>Welcome " + str(userid) + "!</h1>"
        print "<img src='data:image/png;base64,%s' />" % pngdata
        print "<h3>What's in the (in)box?</h3>"
        #display num of unread emails
        #print "<p>You have _ unread emails</p>"
        print "<ul>"
        print "   <li class='navlist'><a href='inbox.py'>Inbox</a></li> &nbsp; | &nbsp; "
        print "   <li class='navlist'><a href='outbox.py'>Outbox</a></li>&nbsp; | &nbsp;  "
        print "   <li class='navlist'><a href='send.py'>Send Email</a></li> "
        print "</ul>"
        print """
        <br>
        <h4><a href='login.py'>Logout</a></h4>
        """
        print "</body>"
        print "</html>"
    
    else:
        #couldn't auth user
        print "Content-type: text/html"
        print
        print "<!doctype html>"
        print "<head>"
        print "<title>Error</title>"
        print "</head>"
        print "<body>"
        print "<h1>You have reached this page in error.  Click <a href='login.py' action='clear.py'>here</a> to login again.</h1>"
        print "</body>"
        print "</html>"

main()
