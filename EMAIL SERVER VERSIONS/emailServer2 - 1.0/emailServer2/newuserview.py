#!/usr/bin/env python
import Cookie
import base64

class NewUserView:
    def view(self):
        print "Content-type: text/html"
        print
        print "<!doctype html>"
        print "<head>"
        print "<title>Brian's e-mail Server</title>"
        print "<link type='text/css' rel='stylesheet' href='./css/style.css'>"
        print "</head>"
        print "<body>"
        print "<h3>Enter new user and password below: </h3>"
        print "<form id='form1' method='post' >"
        print "   <p>User ID:   <input type='text' name='userid' /> </p>"
        print "   <p>Password: <input type='password' name='password' /> </p>"
        print "   <input type='submit' name='newuser' id='newuser' value='newuser'>"
        print "</form>"
        print "<h4><a href='LoginController.py'>Login</a>"
        print "</body>"
        print "</html>"

    def exists(self):
        print "Content-type: text/html"
        print
        print "<!doctype html>"
        print "<html>"
        print "<head>"
        print "<title>Error</title>"
        print "</head>"
        print "<body>"
        print "<p>Error, there is already a user in the system with that username, <a href='NewUserController.py'>try again</a>.</p>"
        print "</body>"
        print "</html>"
        
    def redir(self):
        print "Content-type: text/html"
        print
        print "<!doctype html>"
        print "<html>"
        print "<head>"
        print "<meta http-equiv='Refresh' content='0; url=LoginController.py' />"
        print "</head>"
        print "<body>"
        print "<p>Please follow <a href='LoginController.py'>this link</a>.</p>"
        print "</body>"
        print "</html>"
        print
        print
        
