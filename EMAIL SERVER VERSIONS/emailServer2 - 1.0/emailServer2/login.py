#!/usr/bin/env python
 
import cgi
import cgitb
import sys
import Cookie
import base64


#
# isValid()
#

def isValid(userid, password):
    with open('users.txt', 'r') as file:
        # read the file into lines
        lines = file.readlines() 

        # iterate through lines, splitting each line into strings
        for line in lines:
            strings = iter(line.split())

            # process each string pair, return True if match
            # otherwise when end of file reached, drop to return False
            while True:
                try:
                    fileUserId = strings.next()
                    filePassword = strings.next()

                    # we have a match
                    if (userid == fileUserId):
                        return True

                except StopIteration:
                    break

    
    return False

#
# Main Program
#
def main():
    form = cgi.FieldStorage() 
    userid   = form.getvalue('userid')
    password = form.getvalue('password')

    if isValid(userid, password):
        cookie = Cookie.SimpleCookie()
        cookie['userid']=userid
        cookie['password']=password
		
	print "Content-type: text/html"
        print cookie.output()
	print
        print "<!doctype html>"
        print "<html>"
        print "<head>"
        print "<meta http-equiv='Refresh' content='0; url=profile.py' />"
        print "</head>"
        print "<body>"
        print "<p>Please follow <a href='profile.py'>this link</a>.</p>"
        print "</body>"
        print "</html>"
        print
        print
        
    else:
        jpgdata = base64.b64encode(open("billTed.jpg",'rb').read())
		
	print "Content-type: text/html"
        print
        print "<!doctype html>"
        print "<head>"
        print "<title>Brian's e-mail Server</title>"
	print "<link type='text/css' rel='stylesheet' href='./css/style.css'>"  
        print "</head>"
        print "<body>"
        print "<h1>Welcome to Brian's e-mail server</h1>"
        print "<img src='data:image/jpg;base64,%s' />" % jpgdata
        print "<form id='form' method='post' >"
        print "   <p>User ID:   <input type='text' name='userid' /> </p>"
        print "   <p>Password: <input type='password' name='password' /> </p>"
        print "   <input type='submit' name='submit' value='submit'>"
        print "</form>"
        print "<h4><a href='newUser.py'>New User</a></h4>"
        print "</body>"
        print "</html>"

main()
