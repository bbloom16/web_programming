#!/usr/bin/env python

import cgi
import cgitb
import sys
import Cookie
import os
import base64

#logger
import logging
FORMAT = "[%(filename)s:%(lineno)s - %(funcName)10s() ] %(message)s"
logging.basicConfig(filename='sendPYoutput.log',format=FORMAT)
logger = logging.getLogger('root')
logger.setLevel(logging.DEBUG)

#auth user from cookies passed
def isValid(userid, password):
    with open('users.txt','r') as file:
        lines = file.readlines()

        for line in lines:
            strings = iter(line.split())
            while True:
                try:
                    fileUserId = strings.next()
                    filePass = strings.next()

                    if(userid == fileUserId) and (password == filePass):
                        return True
                except StopIteration:
                    break
    return False
#
#getEmails: creates an array based off the text in the _MailIn file
#

def getEmails(userid):
    emails = []
    with open(str(userid)+'MailIn.txt','r') as file:
        #read in the email file; treat each new line as a new email
        lines = file.readlines()

        for line in lines:
            emails.append(line)
            
    return emails

#
#printEmails: makes a single string for all of the email text for that user, removing any unnecessary formatting
#

def printEmails(emails):
    i = int(0)
    result = ''
    for email in emails:
        line = str(emails[i]).replace('[','').replace(']','').replace("'",'')
        #result += 'Email ' + str(i+1) + ': ' + line +'<br /><br />'
        emailNum = str(i+1)
        result += '<input type="radio" name="emailSelect" value="' + emailNum + '" > Email ' + emailNum + ': ' + line +'<br /><br />'   
        i = i + 1
        
    return result

def viewEmail(emailPassed, userPassed):
    #open inbox for that user
    emails = getEmails(userPassed)
    count = int(emailPassed)-1
    emailShown = emails[count]

    
    print "Content-type: text/html"
    print
    print "<!doctype html>"
    print "<head>"
    print "<title>Email</title>"
    print "</head>"
    print "<body>"
    print "<p>" + str(emailShown) + "</p>"
    print "</body>"
    print "</html>"
    print
    print
    
def deleteEmail(emailPassed, userPassed):
    #get the array of current emails
    emails = getEmails(userPassed)
    count = int(emailPassed)-1

    #open the user's inbox file and re-write the file, without the selected file
    f = open(str(userPassed)+'MailIn.txt','w+')
    i = 0
    for email in emails:
        if(i!=count):
            f.write(emails[i])
        i = i+1
    f.close()

    print "Content-type: text/html"
    print
    print "<!doctype html>"
    print "<html>"
    print "<head>"
    print "<meta http-equiv='Refresh' content='0; url=inbox.py' />"
    print "</head>"
    print "</html>"
    print
    print

#show a drop down menu of all users in the system
def printUsrs():
    users = []
    with open('users.txt','r') as file:
        lines = file.readlines()
    
        for line in lines:
            strings = iter(line.split())
            while True:
                try:
                    userid = strings.next()
                    #do not send password to dropdown menu
                    password = strings.next()
                    users.append(userid)
                    #strings.next()
                except StopIteration:
                    break
    usersPrint = '<select name="userlist">'
    for user in users:
        usersPrint += '<option value="'+ user+ '">' + user + '</option>'
	#logger.debug(usersPrint)
    usersPrint += '</select>'
    #logger.debug(usersPrint)
    return usersPrint
        
#sendEmail
def sendEmail(sender,recip,body):
    #send to sender's outbox
    f = open(str(sender+'MailOut.txt'),'a')
    f.write(body + "\n")
    f.close()
    #send to recip's inbox
    f1 = open(str(recip+'MailIn.txt'),'a')
    f1.write(body + "\n")
    f1.close()

    #refresh page
    print "Content-type: text/html"
    print
    print "<!doctype html>"
    print "<html>"
    print "<head>"
    print "<meta http-equiv='Refresh' content='0; url=send.py' />"
    print "</head>"
    print "</html>"


#main
def main():

    cookie = Cookie.SimpleCookie()
    cookie_string = os.environ.get('HTTP_COOKIE')
    cookie.load(cookie_string)
    userid = cookie['userid'].value
    password = cookie['password'].value
    
    if isValid(userid, password):
        form = cgi.FieldStorage()
        
        #if send button pressed, send email and refresh page
        if "send" in form:
            #send email
            recip = form.getvalue('userlist')
            body = form.getvalue('body')
            #pass recipiant and body to method to send email
            sendEmail(userid,recip,body)
                       
        else:
            #selecting email to view
            form = cgi.FieldStorage()
            emailSelect = form.getvalue('emailSelect')

            #syntax for image
            pngdata = base64.b64encode(open("box.png",'rb').read())
            #method to get all users in system
            result = printUsrs()
            logger.debug(result)
        
            print "Content-type: text/html"
            print
            print "<!doctype html>"
            print "<head>"
            print "<title>Send e-Mail</title>"
            print "<link type= 'text/css' rel='stylesheet' href= './css/style.css'>"	   
            print "</head>"
            print "<body>"
            #identify user by cookie
            print "<h1>Welcome " + str(userid) + "!</h1>"
            #print "<img src='data:image/png;base64,%s' />" % pngdata
            print "<h3>Send an email to a user in our system:</h3>"
            #display num of unread emails
            #print "<p>You have _ unread emails</p>"
            print "<ul>"
            print "   <li class='navlist'><a href='inbox.py'>Inbox</a></li> &nbsp; | &nbsp; "
            print "   <li class='navlist'><a href='outbox.py'>Sent Mail</a></li>&nbsp; | &nbsp;  "
            print "   <li class='navlist'><a href='send.py'>Send Message</a></li> "
            print "</ul>"
            #print the users to select recipiant
            print "<form id='sendEmail' method='post'>"
            print "<p>Recipiant: " + str(result) + "</p>"
            #print str(result)
            #text area body
            print "<br /><textarea name='body' rows='10' cols='50'>Enter text for email here...</textarea>"
            #send email button
            print "<br /><input type='submit' id='send' form='sendEmail' name='send' value='Send' />"
            print "</form>"
            print """
            <br>
            <h4><a href='profile.py'>My profile</a></h4>
            """
            print "</body>"
            print "</html>"
    
    else:
        #couldn't auth user
        print "Content-type: text/html"
        print
        print "<!doctype html>"
        print "<head>"
        print "<title>Error</title>"
        print "</head>"
        print "<body>"
        print "<h1>You have reached this page in error.  Click <a href='login.py'>here</a> to login again.</h1>"
        print "</body>"
        print "</html>"

main()
