#!/usr/bin/env python
import dataset
import logging
from flask import current_app
from mail import Mail

class MailDao:
    def __init__(self, userid):
        user = userid.replace('"', '')
        self.connectString = 'sqlite:///'+user+'mail.db'
        self.db = dataset.connect(self.connectString)
        self.inbox = self.db['inbox']
        self.outbox = self.db['outbox']

        #self.populateInbox()
        #self.populateOutbox()
        
        try:
            self.logger = current_app.logger
        except:
            self.logger = logging.getLogger('root')

        #self.logger.debug('got to MailDao')
            
    def rowToEmail(self, row):
        email = Mail(row['text'], row['emailId'])
        #self.logger.debug(row['text'])
        return email

    def emailToRow(self, email):
        row = dict(text=email.text, emailId=email.emailId)
        return row

    def readInbox(self):
        inbox = self.db['inbox']
        rows = inbox.all()

        result = []
        for row in rows:
            result.append(self.rowToEmail(row))

        return result
    
    def readOutbox(self):
        outbox = self.db['outbox']
        rows = outbox.all()

        result = []
        for row in rows:
            result.append(self.rowToEmail(row))
            
        return result
                                            
    def addInbox(self, email):
        self.inbox.insert(self.emailToRow(email))
        self.db.commit()

    def addOutbox(self, email):
        self.outbox.insert(self.emailToRow(email))
        self.db.commit()

    def deleteInbox(self, email):
        self.inbox.delete(emailId=email.emailId)
        self.db.commit()

    def deleteOutbox(self, email):
        self.outbox.delete(emailId=email.emailId)
        self.db.commit()

    def inboxSelectById(self,emailId):
        rows = self.inbox.find(emailId=emailId)

        if(rows is None):
            self.logger.debug('Failed to find')

            result = None
            return result
        else:
            count = 0
            for row in rows:
                if(count > 0):
                    self.logger.debug('More than one')
                    return None
                else:
                    result = self.rowToEmail(row)
                    count = count + 1
            return result
        
    def outboxSelectById(self,emailId):
        rows = self.outbox.find(emailId=emailId)

        if(rows is None):
            self.logger.debug('Failed to find')

            result = None
            return result
        else:
            count = 0
            for row in rows:
                if(count > 0):
                    self.logger.debug('More than one')
                    return None
                else:
                    result = self.rowToEmail(row)
                    count = count + 1
            return result

    def lastInboxId(self):
        result = 0

        inbox = self.db['inbox']
        rows = inbox.all()

        for row in rows:
            result = (self.rowToEmail(row)).emailId

        return int(result)

    def lastOutboxId(self):
        result = 0

        outbox = self.db['outbox']
        rows = outbox.all()

        for row in rows:
            result = (self.rowToEmail(row)).emailId

        return int(result)

    def populateInbox(self):
        self.inbox.insert(self.emailToRow(Mail('first inbox email',self.lastInboxId()+1)))
        self.inbox.insert(self.emailToRow(Mail('second inbox email',self.lastInboxId()+1)))
        self.inbox.insert(self.emailToRow(Mail('third inbox email',self.lastInboxId()+1)))
        self.db.commit()
        
    def populateOutbox(self):
        self.outbox.insert(self.emailToRow(Mail('first outbox email',self.lastOutboxId()+1)))
        self.outbox.insert(self.emailToRow(Mail('second outbox email',self.lastOutboxId()+1)))
        self.outbox.insert(self.emailToRow(Mail('third outbox email',self.lastOutboxId()+1)))
        self.db.commit()
