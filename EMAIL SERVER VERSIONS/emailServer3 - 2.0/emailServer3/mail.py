class Mail:
    def __init__(self, text, emailId):
        self.text = text
        self.emailId = emailId

    def toString(self):
        return self.text + " " + self.emailId
