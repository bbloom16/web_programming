from jsonpickle import encode
from flask import Flask
from flask import abort, redirect, url_for
from flask import request
from flask import render_template
from flask import session
import sys
import logging
from logging.handlers import RotatingFileHandler
from logging import Formatter
from userdao import UserDao
from user import User
from maildao import MailDao
from mail import Mail
app = Flask(__name__)

@app.route('/')
def index():
    return redirect(url_for('login'))

@app.route('/login', methods=['POST', 'GET'])
def login():
    error = None
    
    if request.method == 'POST':
        if isValid(request.form['userid'],request.form['password']):
            #redir to profile
            userid = str(request.form['userid'])
            session['userid']=encode(userid)
            return profile()
        else:
            error = 'Invalid userid/password'

    return render_template('login.html', error=error)


def isValid(userid, password):
    dao = UserDao()
    user = dao.selectByUserid(userid)
    if (user is not None) and (userid == user.userid) and (password == user.password):
        session['user']=encode(user)
        session['userid']=encode(userid)
        return True
    else:
        return False

@app.route('/profile', methods=['POST', 'GET'])
def profile():
    error = None
    
    userid = session['userid'].replace('"','')

    if ('inbox' in request.form):
        return inbox()

    elif('outbox' in request.form):
        return outbox()

    elif('send' in request.form):
        return send()
    
    else:
        return render_template('profile.html', **locals())

@app.route('/inbox', methods=['POST', 'GET'])
def inbox():
    error = None

    userid = session['userid'].replace('"','')
    mailDao = MailDao(userid)

    if('view' in request.form) and (request.form['emailSelect'] is not None):
        emailSel = request.form['emailSelect']
        result = mailDao.readInbox()
        emailPass = ''
        for email in result:
            if(emailSel==email.emailId):
                emailPass = email
        #encode selected email
        session['emailId'] = encode(emailPass.emailId)
        session['text'] = encode(emailPass.text)
        session['page'] = encode('inbox')
        #redir to viewEmail page
        return viewEmail()
    elif('delete' in request.form) and (request.form['emailSelect'] is not None):
        #delete that email
        emailSel = request.form['emailSelect']
        email = mailDao.inboxSelectById(emailSel)
        mailDao.deleteInbox(email)
        refresh('outbox')
    else:
        emails = mailDao.readInbox()
        #app.logger.debug(emails)
        
    return render_template('inbox.html', **locals())

@app.route('/outbox', methods=['POST', 'GET'])
def outbox():
    error = None

    userid = session['userid'].replace('"','')
    mailDao = MailDao(userid)

    if('view' in request.form) and (request.form['emailSelect'] is not None):
        emailSel = request.form['emailSelect']
        result = mailDao.readOutbox()
        emailPass = ''
        for email in result:
            if(emailSel==email.emailId):
                emailPass = email
        #encode selected email
        session['emailId'] = encode(emailPass.emailId)
        session['text'] = encode(emailPass.text)
        session['page'] = encode('outbox')
        #redir to viewEmail page
        return viewEmail()
    elif('delete' in request.form) and (request.form['emailSelect'] is not None):
        #delete that email
        emailSel = request.form['emailSelect']
        email = mailDao.outboxSelectById(emailSel)
        mailDao.deleteOutbox(email)
    else:
        emails = mailDao.readOutbox()
    
    return render_template('outbox.html', **locals())

@app.route('/viewemail', methods=['POST', 'GET'])
def viewEmail():
    emailId = session['emailId']
    text = session['text']
    return render_template('viewEmail.html', **locals())

@app.route('/send', methods=['POST','GET'])
def send():
    if('sendMail' in request.form):
        recip = request.form['userlist']
        body = request.form['body']
        #put email in sender's outbox
        sendDAO = MailDao(session['userid'])
        emailOut = Mail(body, sendDAO.lastOutboxId()+1)
        sendDAO.addOutbox(emailOut)

        #put email in recip's inbox
        recipDAO = MailDao(recip)
        emailIn = Mail(body, recipDAO.lastInboxId()+1)
        recipDAO.addInbox(emailIn)

    else:
        userDao = UserDao()
        users = userDao.selectAll()
        
    return render_template('send.html', **locals())

@app.route('/newUser', methods=['POST','GET'])
def newUser():
    if('newUser' in request.form):
        userid = request.form['userid']
        password = request.form['password']

        userDao = UserDao()
        userDao.insert(User(userid, password))

        return login()

    else:
        return render_template('newUser.html', **locals())

@app.route('/refresh', methods=['POST','GET'])
def refresh(page):
    return render_template('refresh.html', **locals())

@app.route('/logout', methods=['POST','GET'])
def logout():
    session.clear()

    #for key in session.keys():
        #session.pop(key)
    
    return login()

if __name__ == "__main__":
    app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'
    streamhandler = logging.StreamHandler(sys.stderr)
    streamhandler.setLevel(logging.DEBUG)
    streamhandler.setFormatter(Formatter("[%(filename)s:%(lineno)s - %(funcName)10s() ] %(message)s"))
    app.logger.addHandler(streamhandler)
    app.logger.setLevel(logging.DEBUG)
    app.run(host='0.0.0.0')
