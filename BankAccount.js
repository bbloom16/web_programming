// Transaction for array
function Transaction(name, amount){
    var name = name;
    var amount = amount;
    this.toString = function(){
	return name + " $" + amount + "\n <br/>";
    };

}

//BankAccount parent
function BankAccount (name, balance){
    this.name = name;
    this.balance = balance;
    var first = new Transaction("Account Created with starting balance of ", this.balance);
    var transactions = [first];
    this.getTransactions = function(){
	return transactions;
    };
    this.fixTransactions = function(transactions){
        this.transactions = transactions;
    };
    this.addTransaction = function(newTransaction){
   	transactions[transactions.length] = newTransaction;
    };
    this.getName = function() {
	return this.name;
    };
    this.getBalance = function() {
	return this.balance;
    };
    this.setBalance = function(balance) {
	this.balance = balance;
    };
    this.deposit = function(amount) {
	transactions[transactions.length] = new Transaction("Deposit  of", amount);	
	this.balance = Number(this.balance) + Number(amount);
	return Number(this.balance);
    };
    this.withdrawal = function(amount) {	
	if((Number(this.balance)>Number(amount)) || (Number(this.balance) == Number(amount))){
		transactions[transactions.length] = new Transaction("Withdrawal of ", amount);		
		this.balance = Number(this.balance) - Number(amount);
		return Number(this.balance);
	}
	else{
		transactions[transactions.length] = new Transaction("Unsucessful transaction, insuffificent funds.  Attempted withdrawal of ", amount);		
		return 0;
	}
    };
    this.printTransactions = function() {
	document.write("Account: " + this.name + "<br />");
	document.write("Number of transactions: " + transactions.length + "<br />");	
	for(i in transactions){
		document.write("Transaction " + Number(Number(i)+1) + ": " + transactions[i].toString(this));
	}
	document.write("End of Month Balance: $" + this.balance);
	document.write("<br /><br />");
    };
		     
}

// CheckingAccount
function CheckingAccount(){
    
    // overdraft protection
    this.withdrawal = function(amount) {
 	var transactions = this.getTransactions();
	var len = transactions.length;	
	if((Number(this.balance+500) > Number(amount)) || (Number(this.balance+500) == Number(amount))){		
		transactions[len] = new Transaction("Withdrawal of ", amount);		
		this.balance = Number(this.balance) - Number(amount);
		this.fixTransactions(transactions);
		return Number(this.balance);
	}
	else{		
		return 0;
	}
    };
}

// SavingsAccount
function SavingsAccount(){
    rate = 0.03;
    //compute interest
    this.computeInterest = function() {
	var interest = this.balance*rate;
	return interest;
    };
    this.addInterest = function() {
	var interest = this.computeInterest();
	var message = "Interest Computed on balance of $" + this.balance + ": ";	
	var newTransaction = new Transaction(message, interest);	
	this.addTransaction(newTransaction);
	this.balance = interest + this.balance;
	return interest;
    };
}

// BonusSavingsAccount
function BonusSavingsAccount(){
    var bonusAmt = 5000;
    var bonus = 50;
    var fee = 10;

    this.withdrawal = function(amount){	
	var transactions = this.getTransactions();
	var len = transactions.length;	
	if((Number(this.balance+10) > Number(amount)) || (Number(this.balance+10) == Number(amount))){
		var message = "Withdrawal of $" + amount + " + $" + fee + " fee";		
		transactions[len] = new Transaction(message, (amount+fee));		
		this.balance = Number(this.balance) - Number(amount) - Number(fee);
		this.fixTransactions(transactions);
		return Number(this.balance);
	}
	else{		
		return 0;
	}
    };
   

    this.getBonus = function(){
	if(Number(this.balance)>Number(bonusAmt)){
		return true;
	}
	else{
		return false;
	}
    };
    this.addBonus = function() {
	var checkBonus = this.getBonus();
	if(checkBonus == true){
		var message = "Bonus of $" + bonus + " added to balance of $" + this.balance + ": ";	
		this.balance = bonus + this.balance;
		var newTransaction = new Transaction(message, this.balance);
		this.addTransaction(newTransaction);
		return bonus;
	}
	else{
		return 0;
	}
    };
}

//Initialize prototype chain
//create checking account with starting balance of $200
CheckingAccount.prototype = new BankAccount("My Checking", 200);
CheckingAccount.prototype.contructor = CheckingAccount;
var myChecking = new CheckingAccount();

//create savings account with starting balance of $200
SavingsAccount.prototype = new BankAccount("My Savings", 200);
SavingsAccount.prototype.contructor = SavingsAccount;
var mySavings = new SavingsAccount();

//create a bonus savings account with starting balance of $5300
SavingsAccount.prototype = new BankAccount("My Bonus Savings Account", 5300);
SavingsAccount.prototype.contructor = SavingsAccount;
var bonus1 = new SavingsAccount();
BonusSavingsAccount.prototype = bonus1;
BonusSavingsAccount.prototype.contructor = BonusSavingsAccount;
var myBonus = new BonusSavingsAccount();
//End prototype chain


/*-------------BEGIN MONTH-----------------*/


//make two withdrawals of $150 each
myChecking.withdrawal(150);
myChecking.withdrawal(150);
//make deposit of $50
myChecking.deposit(50);
//make withdrawal of $300
myChecking.withdrawal(300);


//make two withdrawals of $150 each
mySavings.withdrawal(150);
mySavings.withdrawal(150);
//make a deposit of $50
mySavings.deposit(50);
//make a withdrawal of $100
mySavings.withdrawal(100);


//make two withdrawals of $150 each
myBonus.withdrawal(150);
myBonus.withdrawal(150);
//make a deposit of $50
myBonus.deposit(50);
//make a withdrawal of $50
myBonus.withdrawal(50);

/*------------END MONTH------------------*/

//add interest rates
mySavings.addInterest();
myBonus.addInterest();
myBonus.addBonus();

//generate statements
myChecking.printTransactions();
mySavings.printTransactions();
myBonus.printTransactions();
